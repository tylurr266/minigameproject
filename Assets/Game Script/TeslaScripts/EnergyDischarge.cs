﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnergyDischarge : MonoBehaviour {
   public bool dischargeActive = true;

    public GameObject laserPrefab;
	public Button myButton;

	public Animation animation1;

    // Use this for initialization
    void Start()
    {


	}

    // Update is called once per frame
    void Update()
    {

    }

    public void fireLaser()
    {
        GameObject laser = Instantiate(laserPrefab, transform.position, Quaternion.identity) as GameObject;
        laser.GetComponent<PWGTrigger>().setDirection(Vector3.forward);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Vector3 direction = transform.TransformDirection(Vector3.forward) * 10;
        Gizmos.DrawRay(transform.position, direction);
    }
}