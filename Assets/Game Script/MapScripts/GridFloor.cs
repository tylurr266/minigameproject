﻿using UnityEngine;
using System.Collections;

public class GridFloor : MonoBehaviour
{
    Vector3 roundedPos;
	public GameObject gridFloor;
	public float gridX = 5f;
	public float gridY = 5f;
	public float spacing = 2f;

	void Start()
    {
	   // Instantiates a prefab in a grid
		for (int y = 0; y < gridY; y++)
		{
			for (int x = 0; x < gridX; x++)
			{
				Vector3 pos = new Vector3(x, 0, y) * spacing;
				Instantiate(gridFloor, pos, Quaternion.identity);
			}
		}
	}

	void Update()
		{
        
		}
}