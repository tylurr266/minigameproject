﻿using UnityEngine;
using System.Collections;

public class TesseractPosition : MonoBehaviour {
    public bool dirRight = true;
    public float dirSpeed = 0.01f;
	private float distanceTraveled = 0f;
	public float maxDistance = 15f;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		distanceTraveled += dirSpeed * Time.deltaTime;
        if (dirRight)
            transform.Translate(Vector2.right * dirSpeed * Time.deltaTime);
        else
            transform.Translate(Vector2.left * dirSpeed * Time.deltaTime);

        if (distanceTraveled >= maxDistance)
        {
            dirRight = !dirRight;
			distanceTraveled = 0f;
        }
    }
}
