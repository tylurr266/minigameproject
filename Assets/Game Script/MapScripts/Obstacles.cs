﻿using UnityEngine;
using System.Collections;

public class Obstacles : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PWG"))
        {
            Destroy(other.gameObject);
        }
    }
}
