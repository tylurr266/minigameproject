﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GrounderGravityLeft : MonoBehaviour
{
    public List<Transform>  targets;
    public PWGTrigger laserPrefab;


	void Start()
	{
		targets = new List<Transform>();

	}

    void Update()
    {
        if (targets.Count != 0)
        {
			foreach (Transform laser in targets)
			{
				if (laser != null)
				{
					Vector3 relativePos = (transform.position - laser.localPosition);
					Quaternion rotation = Quaternion.LookRotation(relativePos);

					Quaternion current = laser.localRotation;

					laser.localRotation = Quaternion.Slerp(current, rotation, Time.deltaTime);
					laser.Translate(0, 0, 5f * Time.deltaTime);
				}
			}
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PWG"))
        {
			if (!other.gameObject.GetComponent<PWGTrigger>().isCaught)
            { 
				PWGTrigger laser = Instantiate(laserPrefab, other.transform.position, Quaternion.identity) as PWGTrigger;
				laser.isCaught = true;
				targets.Add(laser.transform);
				Destroy(other.gameObject);
			}
		}
    }
}