﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{

	//----------------------------------------
	// handles
	public UIManager UI;

	//-----------------------------------------
	// function definitions
	void Start() {
		UI.GetComponentInChildren<Canvas>().enabled = false;
	}

	public void TogglePauseMenu()
	{
		// not the optimal way but for the sake of readability
		if (!UI.GetComponentInChildren<Canvas>().enabled)
		{
			UI.GetComponentInChildren<Canvas>().enabled = true;
		}
		else
		{
			UI.GetComponentInChildren<Canvas>().enabled = false;
		}
	}

}
