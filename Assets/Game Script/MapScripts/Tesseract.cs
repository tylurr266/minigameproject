﻿using UnityEngine;
using System.Collections;

public class Tesseract : MonoBehaviour {

    public float f = 0.0f;
    public float speed = 45.0f;
    public Vector3 v = Vector3.zero;
    public ParticleSystem pSystem;

    void Update()
    {

        if (f < 720.0f)
        {
            v += new Vector3(1, 1, 1) * Time.deltaTime * speed;
            transform.transform.eulerAngles = v;
            f += Time.deltaTime * speed;
        }
        else
        {
            f = 0.0f;
            v = Vector3.zero;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PWG"))
        {
            Destroy(other.gameObject);
            pSystem.Play();
        }
    }
}