﻿using UnityEngine;
using System.Collections;

public class EnergyTetherRight : MonoBehaviour
{
	public ParticleSystem pSystem;
	public bool dischargeActive = true;

	public GameObject laserPrefab;

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{


	}

	public void fireLaser()
	{
		GameObject laser = Instantiate(laserPrefab, transform.position, Quaternion.identity) as GameObject;
		laser.GetComponent<PWGTrigger>().setDirection(transform.TransformDirection(Vector3.left));
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Vector3 direction = transform.TransformDirection(Vector3.left) * 10;
		Gizmos.DrawRay(transform.position, direction);
	}

	void OnTriggerEnter(Collider other)
	{

		if (other.gameObject.CompareTag("PWG") && dischargeActive)
		{
			fireLaser();
			dischargeActive = false;
			pSystem.Play();
			Destroy(other.gameObject);
		}
	}
}