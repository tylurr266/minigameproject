﻿using UnityEngine;
using System.Collections;

public class RaycastTowerPlace : MonoBehaviour {

	public GameObject prefabTower1;
	public GameObject prefabTower2;
	public GameObject prefabTower3;
	public GameObject prefabTower4;

	public Quaternion direction;

	bool currentFloorOccupied;

	public PutThisShitOnFloor currentfloorSelected;

	// Use this for initialization
	void Start () {
		direction = Quaternion.identity;
    }

	// Update is called once per frame
	void Update() {

		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		
		if (Input.GetMouseButtonDown(0)) {
			if (Physics.Raycast(ray, out hit)) {
				if (hit.collider.CompareTag("FloorTile") && !hit.collider.GetComponent<PutThisShitOnFloor>().occupied) {
					currentfloorSelected = hit.collider.GetComponent<PutThisShitOnFloor>();
					Debug.DrawLine(Camera.main.transform.position, currentfloorSelected.transform.position, Color.cyan, 10.0f);
				}
			}
		}

		if (currentfloorSelected != null &&  !currentfloorSelected.occupied) {

			if (Input.GetKeyDown(KeyCode.A)) {
				setCurrentTowerTo1();
				currentfloorSelected.occupied = true;
			}

			if (Input.GetKeyDown(KeyCode.S)) {
				setCurrentTowerTo2();
				currentfloorSelected.occupied = true;
			}

			if (Input.GetKeyDown(KeyCode.D)) {
				setCurrentTowerTo3();
				currentfloorSelected.occupied = true;
			}

			if (Input.GetKeyDown(KeyCode.F)) {
				setCurrentTowerTo4();
				currentfloorSelected.occupied = true;
			}

		}
	}

	public void setCurrentTowerTo1() {
		Instantiate(prefabTower1, currentfloorSelected.transform.position + new Vector3(0, 2.15f, 0), direction);
    }

	public void setCurrentTowerTo2() {
		Instantiate(prefabTower2, currentfloorSelected.transform.position + new Vector3(0, 2.15f, 0), direction);
	}

	public void setCurrentTowerTo3() {
		Instantiate(prefabTower3, currentfloorSelected.transform.position + new Vector3(0, 1.5f, 0), direction);
	}

	public void setCurrentTowerTo4() {
		Instantiate(prefabTower4, currentfloorSelected.transform.position + new Vector3(0, 0.5f, 0), direction);
	}

	public void setDirectionNorth()
	{
		direction.SetLookRotation(-Vector3.forward);
	}

	public void setDirectionEast()
	{
		direction.SetLookRotation(Vector3.left);
	}

	public void setDirectionSouth()
	{
		direction.SetLookRotation(Vector3.forward);
	}

	public void setDirectionWest()
	{
		direction.SetLookRotation(-Vector3.left);
	}
}
