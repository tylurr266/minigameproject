﻿using UnityEngine;
using System.Collections;

public class TeslaParticle: MonoBehaviour {
    private ParticleSystem pSystem;

    public bool isFired;
    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        
	}

    void Awake()
    {
        pSystem = GetComponent<ParticleSystem>();
    }

    void OnTriggerEnter(Collider other)
    {
        //if (col.gameObject.CompareTag("Laser"))
        //{
        //    transform.position = new Vector3(0, 0, 1);
        //}

        //it'll only enable the emmiter if the object that enters has the right tag
        if (other.gameObject.CompareTag("PWG"))
        {
            pSystem.Play();
            isFired = true;
        }
    }
}
