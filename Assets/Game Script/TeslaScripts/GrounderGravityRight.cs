﻿using UnityEngine;
using System.Collections;

public class GrounderGravityRight : MonoBehaviour
{
    public Transform target;
    bool energyCaught = false;

    void Update()
    {
        if (energyCaught)
        {
            Vector3 relativePos = (target.localPosition - new Vector3(0, 0, 0)) - transform.position;
            Quaternion rotation = Quaternion.LookRotation(relativePos);

            Quaternion current = transform.localRotation;

            transform.localRotation = Quaternion.Slerp(current, rotation, Time.deltaTime);
            transform.Translate(0, 0, 5 * Time.deltaTime);
        }
    }

    //void energyGravity()
    //{
    //    Vector3 relativePos = (target.position + new Vector3(0, 1.5f, 0)) - transform.position;
    //    Quaternion rotation = Quaternion.LookRotation(relativePos);

    //    Quaternion current = transform.localRotation;

    //    transform.localRotation = Quaternion.Slerp(current, rotation, Time.deltaTime);
    //    transform.Translate(0, 0, 3 * Time.deltaTime);
    //}

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("TeslaGrounder"))
        {
            energyCaught = true;
        }
    }
}