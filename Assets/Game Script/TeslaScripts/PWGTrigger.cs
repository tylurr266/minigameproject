﻿using UnityEngine;
using System.Collections;

public class PWGTrigger : MonoBehaviour
{
    public bool isCaught = false;
    public float dirSpeed = 0.01f;

    public Vector3 laserDirection;

    // Use this for initialization
    void Start()
    {
    }

    public void setDirection(Vector3 direction)
    {
        laserDirection = direction;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(laserDirection * dirSpeed * Time.deltaTime);
    }

    void OnDestroy()
    {
        Debug.Log("Laser Destroyed");
    }
    //void OnTriggerEnter(Collider other)
    //{
    //    if (other.gameObject.CompareTag("TeslaCoil"))
    //    {
    //        Destroy(gameObject);
    //    }
    //}
}