﻿using UnityEngine;
using System.Collections;

public class EnergyCore : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PWG"))
        {
            Debug.Log("Success! - Experiment Completed");
            Destroy(other.gameObject);
        }

    }
}
